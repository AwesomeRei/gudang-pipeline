#!/bin/bash
FILES=oas_files/primary/*.yaml
for f in $FILES
do
    echo "Processing $f file..."
    if ! swagger-cli validate $f ; then
        echo $?
        exit 1
    fi
done